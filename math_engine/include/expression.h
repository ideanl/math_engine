#include <iostream>
#include <string>
#include <algorithm>
#include <stack>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

class Expression {
  public:
    Expression(string expression);
    string expr;
    double evaluate();
    vector<string> tokens;
    vector<string> rpn;
    double result;
  private:
    bool isOperator(const string token);
    bool isParenthesis(string token);
    bool isLeftAssociative(string token);
    int cmpPrecedence(string o1, string o2);
    bool toRPN();
    double solveRPN();
    vector<string> getExpressionTokens();
};

Expression::Expression(string expression) {
  expr = expression;
  tokens = getExpressionTokens();
}

bool Expression::isOperator(const string token) {
    return token == "+" || token == "-" ||
            token == "*" || token == "/" ||
            token == "^";
}

bool Expression::isParenthesis(string token) {
    return token == "(" || token == ")";
}

bool Expression::isLeftAssociative(string token) {
    if (token == "^")
      return false;
    else
      return true;
}

int Expression::cmpPrecedence(string o1, string o2) {
    map <string, int> precedenceMap;   
    precedenceMap["+"] = 2;
    precedenceMap["-"] = 2;
    precedenceMap["*"] = 3;
    precedenceMap["/"] = 3;
    precedenceMap["^"] = 4;

    return precedenceMap[o1] - precedenceMap[o2];
}

//Convert to reverse polish notation for easy,efficient computation
bool Expression::toRPN() {
  vector<string> output;
  stack<string> stack;

  bool success = true;

  //for each character
  for(auto i = tokens.begin();i != tokens.end(); i++) {
      if (isOperator(*i)) {
          const string o1 = *i;

          if (!stack.empty()) {
              string o2 = stack.top();

              // While there is an operator token, o2, at the top of the stack AND      
              // either o1 is left-associative AND its precedence is equal to that of o2,      
              // OR o1 has precedence less than that of o2
              while(isOperator(o2) && ((isLeftAssociative(o1)) && (cmpPrecedence(o1, o2) == 0)) || (cmpPrecedence(o1, o2) < 0)) {
                  // Pop off stack and push to output
                  stack.pop();
                  output.push_back(o2);

                  if(!stack.empty())
                      o2 = stack.top();
                  else
                      break;
              }
          }

          stack.push(o1);
      }
      else if (*i == "(") {
          stack.push(*i);
      }
      else if (*i == ")") {
          string top = stack.top();

          // pop from stack and push to output all values until left paren is found
          while(top != "(") {
              output.push_back(top);
              stack.pop();
              if (stack.empty()) break;
              top = stack.top();
          }

          //pop left parentheses
          if (!stack.empty()) stack.pop();

          //if can't find left paren
          if (top != "(")
              return false;
      }
      else {
          output.push_back(*i);
      }
  }

  while(!stack.empty()) {
      string top = stack.top();

      if ((top == "(") || (top == ")"))
          return false;

      output.push_back(top);
      stack.pop();
  }

rpn.assign(output.begin(), output.end());

return success;
}

double Expression::solveRPN() {
    stack<string> stack;
    for(auto i = rpn.begin(); i != rpn.end(); i++) {
        if (isOperator(*i)) {
            double result = 0.0;
            const string val2 = stack.top();
            stack.pop();
            const double n2 = strtod(val2.c_str(), NULL);

            if (!stack.empty()) {
                const string val1 = stack.top();
                stack.pop();
                const double n1 = strtod(val1.c_str(), NULL);

                result = *i == "+" ? n1 + n2 :
                         *i == "-" ? n1 - n2 :
                         *i == "*" ? n1 * n2 :
                         *i == "/" ? n1 / n2 :
                                     pow(n1, n2);
            }
            else {
                if (*i == "-")
                    result = n2 * -1;
                else
                    result = n2;
            }

            ostringstream s;
            s << result;
            stack.push(s.str());
        }
        else {
            stack.push(*i);
        }
    }
    

    return strtod(stack.top().c_str(), NULL);
}

vector<string> Expression::getExpressionTokens() {
    vector<string> tokens;
    string str = "";

    for(int i = 0; i < expr.length(); i++) {
        string token(1, expr[i]);

        if (isOperator(token) || (token == "(") || (token == ")")) {
            if (!str.empty()) {
                tokens.push_back(str);
            }
            str = "";
            tokens.push_back(token);
        }
        else {
            if (!token.empty() && token != "") {
                str.append(token);
            }
            else {
                if (str != "") {
                    tokens.push_back(str);
                    str = "";
                }
            }
        }
    }

    return tokens;
}

//Evaluate an expression
double Expression::evaluate() {
    double expr_result = 0.0;
    if (toRPN()) {
        expr_result = solveRPN();
    }
    result = expr_result;
    return result;
}
