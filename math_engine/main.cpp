//
//  main.cpp
//  math_engine
//
//  Created by Adam Sloma on 9/19/14.
//  Copyright (c) 2014 Adam Sloma and Idean Labib. All rights reserved.
//

#include <iostream>
#include <string>
#include <regex>
#include "include/expression.h"
#include "include/fraction.h"

using namespace std;

//Initial simplifying of equation
void initSimplify(string * left, string * right) {
    smatch matches;
    regex find_parens("\\(.+\\)");

    regex_search(*left, matches, find_parens);
    for(int i = 0; i < matches.size(); i++) {
        Expression expr_left(matches[i]);
        expr_left.evaluate();
        left->replace(matches.position(i), matches[i].length(), to_string(expr_left.result));
    }

    regex_search(*right, matches, find_parens);
    for(int i = 0; i < matches.size(); i++) {
        Expression expr_right(matches[i]);
        expr_right.evaluate();
        right->replace(matches.position(i), matches[i].length(), to_string(expr_right.result));
    }
}


int main(int argc, const char * argv[]) {
    if (argc > 1) {
        string left;
        string right;
        string problem = argv[1];

        problem.erase(remove(problem.begin(), problem.end(), ' '), problem.end()); // strip all spaces

        size_t equal_pos = problem.find("="); // Look for equal sign

        if (equal_pos != string::npos) {
            // EQUATION
            left = problem.substr(0, equal_pos); // (string) Left side of equation
            right = problem.substr(equal_pos + 1); // (string) Right side of equation
            initSimplify(&left, &right);

            cout << left << " = " << right << endl;
        }
    }
    return 0;
}
